# Gunakan base image Windows Server Core
FROM wakeupgoogle/servercore

# Salin installer XAMPP ke dalam image
COPY ./Tools/xampp-5.2 C:\\Tools\\xampp-5.2

# Port yang akan diteruskan (misalnya port Apache)
EXPOSE 80

# Jalankan XAMPP ketika container berjalan
CMD ["C:\\Tools\\xampp-5.2\\apache\\bin\\httpd.exe", "-k", "start", "-DFOREGROUND"]
