<?php
	// ---------------------------------------------------------------------
	// GLOBAL
	// ---------------------------------------------------------------------

	$TEXT['global-xampp'] = "XAMPP for Windows";

	// ---------------------------------------------------------------------
	// NAVIGATION
	// ---------------------------------------------------------------------

	$TEXT['navi-xampp'] = "XAMPP";
	$TEXT['navi-welcome'] = "��ӭ";
	$TEXT['navi-status'] = "״̬";
	$TEXT['navi-security'] = "��ȫ";
	$TEXT['navi-doc'] = "�ĵ�";
	$TEXT['navi-components'] = "���";
	$TEXT['navi-about'] = "����XAMPP";

	$TEXT['navi-demos'] = "��ʾ";
	$TEXT['navi-cdcol'] = "CD��";
	$TEXT['navi-bio'] = "��������";
	$TEXT['navi-guest'] = "Guest Book";
	$TEXT['navi-perlenv'] = "MiniPerl";
	$TEXT['navi-iart'] = "Instant Art";
	$TEXT['navi-iart2'] = "Flash����";
	$TEXT['navi-phonebook'] = "�绰��";
	$TEXT['navi-perlasp'] = "Perl:ASP";
	$TEXT['navi-pear'] = "PEAR:Excel_Writer";
	$TEXT['navi-adodb'] = "ADOdb";
	$TEXT['navi-perl'] = "Perl";
	$TEXT['navi-python'] = "Python";
	$TEXT['navi-jsp'] = "Java";
	$TEXT['navi-phpswitch'] = "PHPת����";

	$TEXT['navi-tools'] = "����";
	$TEXT['navi-phpmyadmin'] = "phpMyAdmin";
	$TEXT['navi-webalizer'] = "Webalizer";
	$TEXT['navi-mercury'] = "Mercury Mail";
	$TEXT['navi-filezilla'] = "FileZilla FTP";
	$TEXT['navi-jpgraph'] = "JpGraph";

	$TEXT['navi-specialguest'] = "��ǰ�û�";
	$TEXT['navi-guest1'] = "FCKeditor";

	$TEXT['navi-languages'] = "����";
	$TEXT['navi-chinese'] = "����";
	$TEXT['navi-german'] = "Deutsch";
	$TEXT['navi-english'] = "English";
	$TEXT['navi-spanish'] = "Espa�ol";
	$TEXT['navi-french'] = "Francais";
	$TEXT['navi-italian'] = "Italiano";
	$TEXT['navi-dutch'] = "Nederlands";
	$TEXT['navi-norwegian'] = "Norsk";
	$TEXT['navi-polish'] = "Polski";
	$TEXT['navi-portuguese'] = "Portugu�s";
	$TEXT['navi-slovenian'] = "Slovenian";

	// ---------------------------------------------------------------------
	// STATUS
	// ---------------------------------------------------------------------

	$TEXT['status-head'] = "XAMPP״̬";
	$TEXT['status-text1'] = "��ҳ���ṩ��������й����������Լ�������Ϣ.";
	$TEXT['status-text2'] = "��ʱ, �޸����û���ɴ����״̬����. ����SSL(https://localhost)��һ���ܿ��ܻ᲻����!";

	$TEXT['status-mysql'] = "MySQL ���ݿ�";
	$TEXT['status-ssl'] = "HTTPS (SSL)";
	$TEXT['status-php'] = "PHP";
	$TEXT['status-perl'] = "Perl with mod_perl";
	$TEXT['status-cgi'] = "ͨ�����ؽӿ� (CGI)";
	$TEXT['status-ssi'] = "Server Side Includes (SSI)";
	$TEXT['status-python'] = "Python with mod_python";
	$TEXT['status-mmcache'] = "PHP��չ��Turck MMCache";
	$TEXT['status-mmcache-url'] = "http://www.apachefriends.org/faq-wampp-en.html#mmcache";
	$TEXT['status-smtp'] = "SMTP ����";
	$TEXT['status-ftp'] = "FTP ����";
	$TEXT['status-tomcat'] = "Tomcat ����";
	$TEXT['status-named'] = "������������ (DNS)";
	$TEXT['status-oci8'] = "PHP��չ��OCI8/Oracle�";
	$TEXT['status-oci8-url'] = "http://www.apachefriends.org/faq-lampp-en.html#oci8";

	$TEXT['status-lookfaq'] = "�鿴FAQ";
	$TEXT['status-ok'] = "�Ѽ���";
	$TEXT['status-nok'] = "δ����";

	$TEXT['status-tab1'] = "���";
	$TEXT['status-tab2'] = "״̬";
	$TEXT['status-tab3'] = "��ʾ";

	// ---------------------------------------------------------------------
	// SECURITY
	// ---------------------------------------------------------------------

	$TEXT['security-head'] = "XAMPP ��ȫ";
	$TEXT['security-text1'] = "��ҳ�ṩ����XAMPP��װ��ȫ״̬�Ŀ��ٸ���. (�벻Ҫ�����Ķ�״̬�����������)";
	$TEXT['security-text2'] = "��ɫ��ǵĲ��ֱ�ʾ��ȫ����ɫ��ǵĲ��ֱ�ʾȷʵ����ȫ����ɫ��ǵĲ��ֱ�ʾ�޷������״̬(Ʃ��, ����������û������)<p>Ҫ�������mysql, phpmyadmin�Լ�xamppĿ¼������, ��ֻ��򵥵�ִ��</b><p>=> <a href=\"/security/xamppsecurity.php\"><b>http://localhost/security/xamppsecurity.php</b></a> <= &nbsp;&nbsp;[ֻ����localhost]<br>&nbsp;<br>&nbsp;<br>
	����һЩ��Ҫ��ʾ:<ul>
	<li>������Щ���Զ�ֻ��������\"localhost\" (127.0.0.1)</li>
	<li><i><b>����FileZilla FTP�Լ�Mercury Mail, �������Լ��޸����еİ�ȫ����!�Դ����Ǳ�ʾ��Ǹ. </b></i></li>
	<li>������ļ����û���������߱�����ǽ�赲, ��ô��Щ������������ⲿ�Ĺ����ǰ�ȫ��.</li>
	<li>�����Щ����û������, ��ô����Ҳ�ǰ�ȫ��!</li></ul>";
	$TEXT['security-text3'] = "<b>��ע������:
	���XAMPP����ȫ, ��ôһЩ���ӽ�����ִ�г���. �������PHP������\"��ȫģʽ(safe mode)\", ��ȫ���������е�һЩ���ܽ����Ṥ��. ͨ����ȫ�빦����ȫ���ܹ���.</b>";
	$TEXT['security-text4'] = "XAMPP �������Ĭ�϶˿�:";

	$TEXT['security-ok'] = "��ȫ";
	$TEXT['security-nok'] = "����ȫ";
	$TEXT['security-noidea'] = "δ֪";

	$TEXT['security-tab1'] = "����";
	$TEXT['security-tab2'] = "״̬";

	$TEXT['security-checkapache-nok'] = "��ЩXAMPPҳ����Ա������е��κ��˷���";
	$TEXT['security-checkapache-ok'] = "��ЩXAMPPҳ�治�����������е��κ��˷���";
	$TEXT['security-checkapache-text'] = "��Ŀǰ����������XAMPP��ʾ����ҳ�����Ա������е��κ��˷���, ֻҪ����֪���������IP��ַ.";

	$TEXT['security-checkmysqlport-nok'] = "MySQL����ͨ���������";
	$TEXT['security-checkmysqlport-ok'] = "MySQL��������ͨ�������";
	$TEXT['security-checkmysqlport-text'] = "�������һЩǱ�ڵĻ������ϵİ�ȫ©��. ������ǳ���ע��ȫ����, ��Ӧ�����ε�MySQl������ӿ�.";

	$TEXT['security-checkpmamysqluser-nok'] = "phpMyAdmin���û�pmaδ��������";
	$TEXT['security-checkpmamysqluser-ok'] = "phpMyAdmin���û�pma����������";
	$TEXT['security-checkpmamysqluser-text'] = "phpMyAdmin��MySQL�б��������Ĳ�������. phpMyAdminͨ��pma�û���������Щ����. ����û�ȱʡ��װʱû����������, Ϊ�˱��ⴥ���κΰ�ȫ����, ��Ӧ��Ϊ����������.";

	$TEXT['security-checkmysql-nok'] = "û��ΪMySQL root�û���������";
	$TEXT['security-checkmysql-ok'] = "MySQL root�û�����������";
	$TEXT['security-checkmysql-out'] = "A MySQL server is not running  or is blocked by a firewall!";
	$TEXT['security-checkmysql-text'] = "�κα����û������Գ�������Ա��Ȩ������������MySQl. ��Ӧ������root����.";

	$TEXT['security-pop-nok'] = "Mercury Mail ������ (POP3) �Ĳ����û�(newuser) ����ʹ�þ����� (wampp)";
	$TEXT['security-pop-ok'] = "POP3 ������(Mercury Mail?) �Ĳ����û� \"newuser\" �����ڻ�������������";
	$TEXT['security-pop-out'] = "POP3 ������, Ʃ��Mercury Mail û�����л򱻷���ǽ��֯!";
	$TEXT['security-pop-notload'] = "<i>��һ��ȫ���Ե���Ҫ��չ�� (IMAP) (php.ini)!</i><br>";
	$TEXT['security-pop-text'] = "���鲢������ͨ��Mercury Mail�������������б༭���е��û��������� !";

	$TEXT['security-checkftppassword-nok'] = "FileZilla FTP������������'wampp'";
	$TEXT['security-checkftppassword-ok'] = "FileZilla FTP�������Ѿ��ı�";
	$TEXT['security-checkftppassword-out'] = "FTP��δ���л򱻷���ǽ����ֹ!";
	$TEXT['security-checkftppassword-text'] = "���FileZilla FTP�������Ѿ�����, Ĭ���û�'newuser'����ͨ��Ĭ������'wampp'��½ϵͳ, �������ϴ���ı�����XAMPP�������ϵ��ļ�. ����, �����׼��ʹ��FileZilla FTP������, ��Ӧ��Ϊ'newuser'����һ���µ�����..";

	$TEXT['security-phpmyadmin-nok'] = "PhpMyAdmin����ͨ�����类�������";
	$TEXT['security-phpmyadmin-ok'] = "PhpMyAdmin�����¼�ѱ�����.";
	$TEXT['security-phpmyadmin-out'] = "PhpMyAdmin: �Ҳ��������ļ� 'config.inc.php' ...";
	$TEXT['security-phpmyadmin-text'] = "PhpMyAdminδ��������, ����ͨ�����类�������. ��PhpMyAdmin�������ļ��е�'auth_type'����Ϊ'httpd'����'cookie'���Ա����������.";

	$TEXT['security-checkphp-nok'] = "PHPδ������\"��ȫģʽ(safe mode)\"";
	$TEXT['security-checkphp-ok'] = "PHP������\"��ȫģʽ(safe mode)\"";
	$TEXT['security-checkphp-out'] = "���ܿ���PHP����!";
	$TEXT['security-checkphp-text'] = "�����ϣ���ṩ����ִ��PHP��Ȩ��, �뿼��ʹ�� \"��ȫģʽ(safe mode)\". ���ڶ���������, �����Ƽ���Ҫ������\"��ȫģʽ(safe mode)\"��, ��Ϊ�����Ļ�, һЩ��Ҫ�ĺ��������ܹ���. <a href=\"http://www.php.net/features.safe-mode\" target=\"_new\"><font size=1>����ϸ��</font></a>";


	// ---------------------------------------------------------------------
	// SECURITY SETUP
	// ---------------------------------------------------------------------

	$TEXT['mysql-security-head'] = "MySQL ��ȫ����̨ & XAMPP Ŀ¼����";
	$TEXT['mysql-rootsetup-head'] = "MYSQL ����: \"ROOT\" ����";
	$TEXT['mysql-rootsetup-text1'] = "";
	$TEXT['mysql-rootsetup-notrunning'] = "MySQL������δ���л��߱�����ǽ����ֹ! ���ȼ�� ...";
	$TEXT['mysql-rootsetup-passwdnotok'] = "The new password is identical with the repeat password. Please enter both passwords for new!";
	$TEXT['mysql-rootsetup-passwdnull'] = "������ ('') ��������!";
	$TEXT['mysql-rootsetup-passwdsuccess'] = "�ɹ�: �����û� 'root' �����������û����!
	����ע��: Ҫʹ���µ�\"root\"������Ҫ��������MYSQL !!!! �µ�������Ϣ��������������ļ���:";
	$TEXT['mysql-rootsetup-passwdnosuccess'] = "����: root�����д���. MySQL �ܾ��˵�ǰ��¼��root�û�����.";
	$TEXT['mysql-rootsetup-passwdold'] = "��ǰ����:";
	$TEXT['mysql-rootsetup-passwd'] = "������:";
	$TEXT['mysql-rootsetup-passwdrepeat'] = "�ظ�������:";
	$TEXT['mysql-rootsetup-passwdchange'] = "�ı�����";
	$TEXT['mysql-rootsetup-phpmyadmin'] = "PhpMyAdmin ��֤:";
	$TEXT['mysql-passwort-infile'] = "Safe plain password in text file?";
	$TEXT['mysql-passwort-risk'] = "Security risk!";

	$TEXT['xampp-setup-head'] = "XAMPPĿ¼���� (.htaccess)";
	$TEXT['xampp-setup-user'] = "�û�:";
	$TEXT['xampp-setup-passwd'] = "����:";
	$TEXT['xampp-setup-start'] = "����XAMPP�ļ���";
	$TEXT['xampp-setup-notok'] = "<br><br>����: �û����Լ����볤��Ϊ3���ַ���15���ַ�.�����ַ�, Ʃ�� <��� (usw.) �Լ����ַ���������!<br><br>";
	$TEXT['xampp-setup-ok'] = "<br><br>The root password was successfully changed. Please restart MYSQL for loading these changes!<br><br>";
	$TEXT['xampp-config-ok'] = "<br><br>�ɹ�: XAMPPĿ¼���ܵ�����! ���еĸ�������All personal data was safed in the following file:<br>";
	$TEXT['xampp-config-notok'] = "<br><br>����: ����ϵͳ����ͨ�� \".htaccess\" �Լ� \"htpasswd.exe\" ������Ŀ¼����. Ҳ������ PHP ������ \"��ȫģʽ(Safe Mode)��\". <br><br>";

	// ---------------------------------------------------------------------
	// START
	// ---------------------------------------------------------------------

	$TEXT['start-head'] = "��ӭʹ��XAMPP for Windows";

	$TEXT['start-subhead'] = "ף����:<br>���Ѿ��ɹ���װ��XAMPP!";

	$TEXT['start-text-newest'] = "";
	
	$TEXT['start-text1'] = "���������Կ�ʼʹ��Apache�Լ����������. ����, ������ͨ�����ĵ������ϵ�'״̬'�������鿴�����Ƿ�������.";

	$TEXT['start-text2'] = "<b>��лCarsten Wiedmann�ṩ��PHP 4.3.10��MySQL 4.1.8��֧���Լ���ΪWebalizer win32�汾�ṩ�Ĳ���! :-)</b>";

	$TEXT['start-text3'] = "";

	$TEXT['start-text4'] = "������ͨ����� <a href='https://127.0.0.1' target='_top'>https://127.0.0.1</a> ���� <a href='https://localhost' target='_top'>https://localhost</a> ����֤OpenSSL";

	$TEXT['start-text5'] = "����Ҫ��һ��! �ǳ���л����Carsten, Nemesis, KriS, Boppy, Pc-Dummy�Լ�XAMPP������֧�ֵ���������!";

	$TEXT['start-text6'] = "ף������, Kay Vogelgesang, Carsten Wiedmann + Kai 'Oswald' Seidler";

	// ---------------------------------------------------------------------
	// MANUALS
	// ---------------------------------------------------------------------

	$TEXT['manuals-head'] = "�����ĵ�";

	$TEXT['manuals-text1'] = "XAMPP�����ֲ�ͬ���������ۺ�Ϊһ����, ����������һЩ��Ҫ��������׼�Ͳο��ĵ����б�.";


	$TEXT['manuals-list1'] = "
	<ul>
	<li><a href=\"http://httpd.apache.org/docs/2.2/en/\">Apache 2 documentation</a>
	<li><a href=\"http://www.php.net/manual/en/\">PHP <b>referenz </b>documentation</a>
	<li><a href=\"http://perldoc.perl.org/\">Perl documentation</a>
	<li><a href=\"http://dev.mysql.com/doc/refman/5.0/en/index.html\">MySQL documentation</a>
	<li><a href=\"http://phplens.com/adodb/\">ADODB</a>
	<li><a href=\"http://eaccelerator.net/DocumentationUk/\">eAccelerator</a>
	<li><a href=\"http://www.fpdf.org/en/doc/index.php\">FPDF Class documentation</a>
	</ul>";

	$TEXT['manuals-text2'] = "һ���򵥵Ľ̳��б��Լ�Apache Friends���ĵ�ҳ:";

	$TEXT['manuals-list2'] = "
	<ul>
	<li><a href=\"http://www.apachefriends.org/en/faq-xampp.html\">Apache Friends documentation</a>
	<li><a href=\"http://www.freewebmasterhelp.com/tutorials/php/\">PHP Tutorial</a> by David Gowans
	<li><a href=\"http://www.davesite.com/webstation/html/\">HTML - An Interactive Tutorial For Beginners</a> by Dave Kristula
	<li><a href=\"http://www.comp.leeds.ac.uk/Perl/start.html\">Perl Tutorial</a> by Nik Silver
	</ul>";

	$TEXT['manuals-text3'] = "ף�����˲�����ÿ��� :)";

	// ---------------------------------------------------------------------
	// COMPONENTS
	// ---------------------------------------------------------------------

	$TEXT['components-head'] = "XAMPP ���";

	$TEXT['components-text1'] = "XXAMPP�����ֲ�ͬ���������ۺ�Ϊһ����, �����������������ĸ�Ҫ.";

	$TEXT['components-text2'] = "��л��Щ����Ŀ�����Ա.";

	$TEXT['components-text3'] = "��D:\Tools\xampp-5.2\mpp\licenses</b>Ŀ¼��, �����ҵ���Щ���������֤(license)�ļ��Լ�README�ļ�.";

	// ---------------------------------------------------------------------
	// CD COLLECTION DEMO
	// ---------------------------------------------------------------------

	$TEXT['cds-head'] = "CD��(PHP+MySQL+PDF��)";

	$TEXT['cds-text1'] = "һ���ܼ򵥵�CD����.";

	$TEXT['cds-text2'] = "CD list as <a href='$_SERVER[PHP_SELF]?action=getpdf'>PDF document</a>.";

	$TEXT['cds-error'] = "�������ݿ�ʧ�ܣ�<br>��ȷ��MySQL�������л�����û�иı�����?";
	$TEXT['cds-head1'] = "�ҵ�CD��";
	$TEXT['cds-attrib1'] = "������";
	$TEXT['cds-attrib2'] = "����";
	$TEXT['cds-attrib3'] = "��";
	$TEXT['cds-attrib4'] = "����";
	$TEXT['cds-sure'] = "ȷ��?";
	$TEXT['cds-head2'] = "����CD";
	$TEXT['cds-button1'] = "ɾ��CD";
	$TEXT['cds-button2'] = "����CD";

	// ---------------------------------------------------------------------
	// BIORHYTHM DEMO
	// ---------------------------------------------------------------------

	$TEXT['bio-head'] = "��������(PHP+GD";

	$TEXT['bio-by'] = "by";
	$TEXT['bio-ask'] = "����д��������";
	$TEXT['bio-ok'] = "OK";
	$TEXT['bio-error1'] = "����";
	$TEXT['bio-error2'] = "���Ϸ�";

	$TEXT['bio-birthday'] = "Birthday";//"����";
	$TEXT['bio-today'] = "Today";//"����";
	$TEXT['bio-intellectual'] = "Intellectual";//"����";
	$TEXT['bio-emotional'] = "Emotional";//"���";
	$TEXT['bio-physical'] = "Physical";//"���";

	// ---------------------------------------------------------------------
	// INSTANT ART DEMO
	// ---------------------------------------------------------------------

	$TEXT['iart-head'] = "��ʱ Art (PHP+GD+FreeType)";
	$TEXT['iart-text1'] = "Font �AnkeCalligraph� by <a class=blue target=extern href=\"http://www.anke-art.de/\">Anke Arnold</a>";
	$TEXT['iart-ok'] = "OK";

	// ---------------------------------------------------------------------
	// FLASH ART DEMO
	// ---------------------------------------------------------------------

	$TEXT['flash-head'] = "Flash����(PHP+MING)";
	$TEXT['flash-text1'] = "MING��win32�汾�Ѿ������ṩ.<br>���Ķ�: <a class=blue target=extern href=\"http://www.opaque.net/wiki/index.php?Ming\">Ming - an SWF output library and PHP module</a>";
	$TEXT['flash-ok'] = "OK";

	// ---------------------------------------------------------------------
	// PHONE BOOK DEMO
	// ---------------------------------------------------------------------

	$TEXT['phonebook-head'] = "�绰��(PHP+SQLite)";

	$TEXT['phonebook-text1'] = "һ���ǳ��򵥵ĵ绰���̱�, ����Ӧ����һ���ǳ��Ƚ������ڿ��ٸ��µļ���: SQLite, һ��û�з���˵�SQL���ݿ�.";

	$TEXT['phonebook-error'] = "�����ݿ�ʧ��!";
	$TEXT['phonebook-head1'] = "�ҵĵ绰���뼯";
	$TEXT['phonebook-attrib1'] = "��";
	$TEXT['phonebook-attrib2'] = "��";
	$TEXT['phonebook-attrib3'] = "�绰����";
	$TEXT['phonebook-attrib4'] = "����";
	$TEXT['phonebook-sure'] = "ȷ��?";
	$TEXT['phonebook-head2'] = "������ϵ��";
	$TEXT['phonebook-button1'] = "ɾ��";
	$TEXT['phonebook-button2'] = "����";

	// ---------------------------------------------------------------------
	// ABOUT
	// ---------------------------------------------------------------------

	$TEXT['about-head'] = "����XAMPP";

	$TEXT['about-subhead1'] = "�뷨��ʵ��";

	$TEXT['about-subhead2'] = "���";

	$TEXT['about-subhead3'] = "����";

	$TEXT['about-subhead4'] = "��ϵ��";

	// ---------------------------------------------------------------------
	// CODE
	// ---------------------------------------------------------------------

	$TEXT['srccode-in'] = "���Դ����";

	// ---------------------------------------------------------------------
	// MERCURY
	// ---------------------------------------------------------------------

	$TEXT['mail-head'] = "ʹ��Mercury Mail SMTP �Լ� POP3 Server ���͵����ʼ�";
	$TEXT['mail-hinweise'] = "һЩ��Ҫ��Mercuryʹ��ָ��!";
	$TEXT['mail-adress'] = "������:";
	$TEXT['mail-adressat'] = "�ռ���:";
	$TEXT['mail-cc'] = "CC:";
	$TEXT['mail-subject'] = "����:";
	$TEXT['mail-message'] = "��Ϣ:";
	$TEXT['mail-sendnow'] = "��Ϣ���ڷ��� ...";
	$TEXT['mail-sendok'] = "��Ϣ���ͳɹ�!";
	$TEXT['mail-sendnotok'] = "����! ��Ϣ����ʧ��!";
	$TEXT['mail-help1'] = "Mercuryʹ��ָ��:<br><br>";
	$TEXT['mail-help2'] = "<ul>
	<li>Mercury ������ʱ��Ҫһ���ⲿ����;</li>
	<li>������ʱ, Mercury �Զ������ṩ������������Ϊ������������(DNS);</li>
	<li>��������ʹ�����ط��������û�: ��ͨ��TCP/IP��������DNS (f.e. 198.41.0.4);</li>
	<li>Mercury �������ļ���Ϊ MERCURY.INI;</li>
	<li>�����Է���һ����Ϣ��postmaster@localhost ���� admin@localhost ���������Բ���������ļ��в����յ�����Ϣ: xampp.../mailserver/MAIL/postmaster ���� (...)/admin;</li>
	<li>Ĭ�ϵĲ����û���Ϊ \"newuser\" (newuser@localhost), ���û�������Ϊ wampp;</li>
	<li>�����ʼ��Լ�����������Ե���Ϣ������Mercury�ܾ�!;</li>
	</ul>";
	$TEXT['mail-url'] = "<a href=\"http://www.pmail.com/overviews/ovw_mercury.htm\" target=\"_top\">http://www.pmail.com/overviews/ovw_mercury.htm</a>";
	// ---------------------------------------------------------------------
	// FileZilla FTP
	// ---------------------------------------------------------------------

	$TEXT['filezilla-head'] = "FileZilla FTP ������";
	$TEXT['filezilla-install'] = "Apache <U>����</U>һ��FTP ������ ... ��FileZilla FTP��. ���Ķ����µĲο�.";
	$TEXT['filezilla-install2'] = "����xampp����Ŀ¼, ִ��\"filezilla_setup.bat\"��������FileZilla. ע��: ����Windows NT, 2000 and XP Professional, FileZilla��Ҫ��װ����.";
	$TEXT['filezilla-install3'] = "���� \"FileZilla FTP\". ������ʹ��FileZilla �ӿ�(\"FileZilla Server Interface.exe\") . ���������������ʹ����2���û������в���:<br><br>
	A: һ��Ĭ���û� \"newuser\", ����Ϊ \"wampp\". �û�Ŀ¼Ϊ xampp\htdocs.<br>
	B: һ�������û� \"anonymous\", ����Ϊ��. �û�Ŀ¼Ϊ xampp\anonymous.<br><br>
	Ĭ�Ͻӿ��ǻػ���ַ 127.0.0.1.";
	$TEXT['filezilla-install4'] = "������ͨ��ִ�� \"FileZillaFTP_stop.bat\" ��ֹͣ FileZilla. ���ϣ�� FileZilla FTP ��Ϊϵͳ����, ��ֱ��ִ�� \"FileZillaServer.exe\". ����, �������������е�����ѡ��.";
	$TEXT['filezilla-url'] = "<br><br><a href=\"http://filezilla.sourceforge.net\" target=\"_top\">http://filezilla.sourceforge.net</a>";

	// ---------------------------------------------------------------------
	// PEAR
	// ---------------------------------------------------------------------

	$TEXT['pear-head'] = "PEAR��Excel�� (PHP)";
	$TEXT['pear-text'] = "�򵥵�<a class=blue target=extern href=\"http://www.contentmanager.de/magazin/artikel_310-print_excel_export_mit_pear.html\">�ֲ�</A>(���İ�)������<a class=blue target=extern href=\"http://www.thinkphp.de/\">ThinkPHP</A>�� Bj�rn Schotte �ṩ";
	$TEXT['pear-cell'] = "�е�ֵ";

	// ---------------------------------------------------------------------
	// JPGRAPH
	// ---------------------------------------------------------------------

	$TEXT['jpgraph-head'] = "JpGraph - PHP��ͼ�ο�";
	$TEXT['jpgraph-url'] = "<br><br><a href=\"http://www.aditus.nu/jpgraph/\" target=\"_top\">http://www.aditus.nu/jpgraph/</a>";

	// ---------------------------------------------------------------------
	// ADODB
	// ---------------------------------------------------------------------

	$TEXT['ADOdb-head'] = "ADOdb - ��һ��DB������ (PHP)";
	$TEXT['ADOdb-text'] = "ADOdb������Active���ݶ�������ݿ�. Ŀǰ֧��MySQL, PostgreSQL, Interbase, Firebird, Informix, Oracle, MS SQL 7, Foxpro, Access, ADO, Sybase, FrontBase, DB2, SAP DB, SQLite�Լ�ͨ��ODBC. The Sybase, Informix, FrontBase and PostgreSQL����������������. �������� \(mini)xampp\php\pear\adodb �ҵ���.";
	$TEXT['ADOdb-example'] = "����:";
	$TEXT['ADOdb-dbserver'] = "���ݿ������ (MySQL, Oracle ..?)";
	$TEXT['ADOdb-host'] = "DB���������� (��������IP)";
	$TEXT['ADOdb-user'] = "�û��� ";
	$TEXT['ADOdb-password'] = "����";
	$TEXT['ADOdb-database'] = "��ǰ���ݿ�����������ݿ�";
	$TEXT['ADOdb-table'] = "���ݿ��еı�";
	$TEXT['ADOdb-nottable'] = "<p><b>��������!</b>";
	$TEXT['ADOdb-notdbserver'] = "<p><b>��ѡ�������������, Ҳ������ҪODBC, ADO, ����OLEDB����!</b>";


	// ---------------------------------------------------------------------
	// INFO
	// ---------------------------------------------------------------------

	$TEXT['info-package'] = "������";
	$TEXT['info-pages'] = "Pages";
	$TEXT['info-extension'] = "��չ";
	$TEXT['info-module'] = "Apacheģ��";
	$TEXT['info-description'] = "����";
	$TEXT['info-signature'] = "ǩ��";
	$TEXT['info-docdir'] = "�ĵ���";
	$TEXT['info-port'] = "Default port";
	$TEXT['info-service'] = "����";
	$TEXT['info-examples'] = "����";
	$TEXT['info-conf'] = "�����ļ�";
	$TEXT['info-requires'] = "��Ҫ";
	$TEXT['info-alternative'] = "Alternative";
	$TEXT['info-tomcatwarn'] = "����! Tomcat��δ������8080�˿�.";
	$TEXT['info-tomcatok'] = "OK! Tomcat�ѳɹ�������8080�˿�.";
	$TEXT['info-tryjava'] = "The java example (JSP) with Apache MOD_JK.";
	$TEXT['info-nococoon'] = "����! Tomcatδ������8080�˿�. ������Tomcatδ������״̬�°�װ
	\"Cocoon\"!";
	$TEXT['info-okcocoon'] = "Ok! Tomcat�Ѿ���������. ��װ�����������������! Ҫ��װ \"Cocoon\" �������� ...";

	// ---------------------------------------------------------------------
	// PHP Switch
	// ---------------------------------------------------------------------

	$TEXT['switch-head'] = "PHP Switch 1.0 win32 for XAMPP";
	$TEXT['switch-phpversion'] = "<i><b>��ǰXAMPP��PHP�汾�� ";
	$TEXT['switch-whatis'] = "<b>WPHP switch��ʲô?</b><br>apachefriendsΪXAMPP�ṩ��PHP Switch ��������PHP4��PHP5֮������л�. �������ڲ�ͬ�汾��PHP�²������Ĵ���.<p>";
	$TEXT['switch-find'] = "<b>PHP Switch������?</b><br>PHP Switch��ִ��һ������\"php-switch.php\"php���� (��XAMPP�İ�װĿ¼). �����Կ���ͨ�������������������ִ��: ";
	$TEXT['switch-care'] = "<b>What can be difficult?</b><br>PHP Switch������2������²�����, a) Apache�������� b) ��XAMPP��װ·���µ�\".phpversion\"�ļ��ǿհ׵Ļ�������. ���ļ�\".phpversion\"�м�¼���ǵ�ǰXAMPPʹ�õ�PHP�汾����, Ʃ��\"4\"����\"5\". ����ִ��Apache��\"shutdown\"����, Ȼ����ִ��\"php-switch.bat\".<p>";
	$TEXT['switch-where4'] = "<b>ִ�к�! �ҵľ������ļ�������?</b><br><br>����PHP 4:<br>";
	$TEXT['switch-where5'] = "<br><br>����PHP 5:<br>";
	$TEXT['switch-make1'] = "<b>�ҵ������ļ��ı�����Щ?</b><br><br>����PHP4 �� PHP5 ��˵���Ƕ�������! <br>";
	$TEXT['switch-make2'] = "<br><br> .. PHP4 ...<br>";
	$TEXT['switch-make3'] = "<br><br> .. PHP5 ...<br>";
	$TEXT['switch-make4'] = "<br><br>ͨ��PHP switching�����ֻص���ԭ����λ��!!<p>";
	$TEXT['switch-not'] = "<b>�ҵ�PHP okay��, �Ҳ�����Ҫһ��\"switch\"�� !!!</b><br>��ô! ��������� ... ;-)<br>";

	// ---------------------------------------------------------------------
	// Cocoon
	// ---------------------------------------------------------------------

	$TEXT['go-cocoon'] = "������������� http://localhost/cocoon/ ������Cocoon";
	$TEXT['path-cocoon'] = "Cocoon ����Ӳ�����D:\Tools\xampp-5.2\��: ...\\xampp\\tomcat\\webapps\\cocoon";

	// ---------------------------------------------------------------------
	// Guest
	// ---------------------------------------------------------------------

	$TEXT['guest1-name'] = "��ǰ�汾���û�: <i>FCKeditor</i>";
	$TEXT['guest1-text1'] = "һ��JavaScriptд�ķǳ�����HTML���߱༭��. ��ΪIE�����Ż�. ������֧��Mozilla FireFox.";
	$TEXT['guest1-text2'] = "FCKeditor����ҳ: <a href=\"http://www.fckeditor.net\" target=\"_new\">www.fckeditor.net</a>. ע��: Ŀǰ��֧��Arial����, ���Ҳ�֪��Ϊʲô!";
	$TEXT['guest1-text3'] = "<a href=\"guest-FCKeditor/fckedit-dynpage.php\" target=\"_new\">FCKeditor������.</A>";
?>
